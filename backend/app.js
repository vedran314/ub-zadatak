const express = require("express");
// const cookieParser = require("cookie-parser");
const logger = require("morgan"); // slusa callove, debuging
const cors = require("cors"); // preventa cors errore

const contactRouter = require("./routes/contact");

const app = express();

app.use(cors())
app.use(logger("dev"));

// This is a built-in middleware function in Express.
// It parses incoming requests with JSON payloads and is based on body-parser.
app.use(express.json());

// It parses incoming requests with urlencoded payloads and is based on body-parser.
app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());

app.use("/API/contact", contactRouter);

module.exports = app;
