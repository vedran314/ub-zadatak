const express = require("express");
const { body, validationResult } = require("express-validator");
const router = express.Router();

router.post(
  "/",
  [
    // email must be a valid email
    body("email").isEmail(),
    // message must be at least 30 chars long
    body("message").isLength({ min: 30 }),
  ],
  function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const { email, message } = req.body;
    console.log(`From: ${email}`);
    console.log(`Message:${message}`);

    return res.json("Your message has been sent!");
  }
);

module.exports = router;
