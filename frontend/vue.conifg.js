const GoogleFontsPlugin = require("google-fonts-webpack-plugin");

module.exports = {
  configureWebpack: {
    plugins: [
      new GoogleFontsPlugin({
        fonts: [{ family: "Open Sans", variants: ["300", "400", "600"] }]
      })
    ]
  },
  chainWebpack: config => {
    config.module
      .rule("vue")
      .use("vue-svg-inline-loader")
      .loader("vue-svg-inline-loader")
      .options({
        /* ... */
      });
  }
};
